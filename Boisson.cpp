//
//  Boisson.cpp
//  intra
//
//  Created by Martine Bellaiche on 2019-03-10.
//  Copyright © 2019 Martine Bellaiche. All rights reserved.
//

#include "Boisson.h"
// Constructeur par parametre de Boisson qui derive de la classe Taxable, toutefois, Boisson comporte des attributs 
// privees supplementaire comme nom_, pri_ et taxe_. Afin de set la taxe, on fait appelle a setTaxe dans le constructeur.
Boisson::Boisson(string_view nom, double prix) : nom_(nom), prix_(prix)
{
	setTaxe();

}


string_view Boisson::getNom() const
{
	return nom_;
}

double Boisson::getPrix()  const
{
	return prix_;
}

// La methode setTaxe ne prend rien en parametre et la fonction de cette methode est de set la taxe a 0.12. Cette methode 
// est definie override puisqu'on modifie la definition originale dans taxable.
void Boisson::setTaxe()
{
	taxe_ = 0.12;
}

// La methode getTaxe ne prend rien en parametre et la fonction de cette methode est de retourner la valeur de l'attribut
// privee taxe_. Encore une fois, on override cette methode puisqu'on doit la redefinir.
double Boisson::getTaxe() const
{
	return taxe_;
}
