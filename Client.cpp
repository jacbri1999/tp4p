/*
* Date : 23 mars 2019
* Auteurs : Matthew Paoli et Jacob Brisson
*/

#include "Client.h"

Client::Client()
{ tableOccupee_ = nullptr;
}

Client::Client(string_view nom, string_view prenom, int tailleGroupe) :
	nom_(nom), prenom_(prenom), tailleGroupe_(tailleGroupe)
{
    tableOccupee_ = nullptr;
}

Client::~Client() {
	//Pas de delete pour la tableOccupee, puisque c'est une agregation par pointeur.
}

int Client::getTailleGroupe() const
{
	return tailleGroupe_;
}

string Client::getNom() const
{
	return nom_;
}

string Client::getPrenom() const
{
	return prenom_;
}
void Client:: setTable(Table * ta)
{ tableOccupee_ = ta;}

Table * Client:: getTable() const
{ return tableOccupee_;}

// M�thode qui prend un ostream par reference en parametre et qui affiche les attributs du Client. 
// C'est une methode virtuelle pure, afin que la bonne methode soit appelee en fonction du type de l'objet.
void Client::afficherClient(ostream & os) const
{ 
	os << prenom_ << " " << nom_;
	if (tableOccupee_ != nullptr) {
		os << " " << *tableOccupee_; 
	}
	os << endl;
}

