/*
* Date : 23 mars 2019
* Auteurs : Matthew Paoli et Jacob Brisson
*/
#ifndef CLIENT_H
#define CLIENT_H

#include <string>
#include <string_view>
#include <iostream>
#include "Table.h"
#include "def.h"
using std::string, std::string_view, std::ostream;  //? On ne devrait normalement pas faire de "using" global dans un .h, mais c'est accepte en INF1010.

class Restaurant;
class Table;
class Client
{
public:
	//Constructeurs
	Client();
	Client(string_view nom, string_view prenom, int tailleGroupe);
	virtual ~Client();

	//getters
	int getTailleGroupe() const;
	string getNom() const;
	string getPrenom() const;
	Table * getTable()const;
	virtual double getReduction(const Restaurant & res, double montant, bool estLivraison) const = 0; // Virtuelle pure, afin que la classe soit abstraite et que cette methode soit redefinie a chaque classe derivee
	virtual int getNbPoints() const = 0; // Virtuelle pure, afin que la classe soit abstraite et que cette methode soit redefinie a chaque classe derivee
	// setters
	void setTable(Table * ta);
	//affichage
	virtual void afficherClient(ostream & os) const = 0; // Virtuelle pure, afin que la classe soit abstraite et que cette methode soit redefinie a chaque classe derivee 

protected:
	string nom_;
	string prenom_;
	int tailleGroupe_;
	Table * tableOccupee_;
};
#endif

