/*
* Date : 23 mars 2019
* Auteurs : Matthew Paoli et Jacob Brisson 
*/
#include "ClientOccasionnel.h"

// Constructeur par parametre de ClientOccasionnel. ClientOccasionnel h�rite de Client. 
ClientOccasionnel::ClientOccasionnel(string_view nom, string_view prenom, int tailleGroupe) : Client(nom, prenom, tailleGroupe)
{ 
}


// Methode qui retourne un nombre de points de 0, etant donne que ClientOcassionnel n'a pas acces aux points de fidelites. Elle redefinie la methode de Client.
int ClientOccasionnel::getNbPoints() const
{
	return 0;
}

// M�thode qui prend un ostream par reference en parametre et qui affiche les attributs du ClientOccasionnel. 
// C'est une methode virtuelle, afin que la bonne methode soit appelee en fonction du type de l'objet.
void ClientOccasionnel::afficherClient(ostream & os) const
{
	Client::afficherClient(os);
}

// Methode qui retourne une reduction de 0.0, puisqu'un ClientOcassionnel n'a pas de reduction. Elle redefinie la methode de Client.
double ClientOccasionnel::getReduction(const Restaurant & res, double montant, bool estLivraison) const
{
	return 0.0;
}
