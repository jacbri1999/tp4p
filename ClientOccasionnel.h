/*
* Date : 23 mars 2019
* Auteurs : Matthew Paoli et Jacob Brisson
*/
#ifndef CLIENT_OCCASIONNEL
#define CLIENT_OCCASIONNEL

#include "Client.h"

class ClientOccasionnel : public Client
{
public:
	//Constructeurs
	ClientOccasionnel(string_view nom, string_view prenom, int tailleGroupe);
	virtual ~ClientOccasionnel() = default;
	//Getters
	int getNbPoints() const override;
	double getReduction(const Restaurant & res, double montant, bool estLivraison) const override; 
	//Autres
	void afficherClient(ostream & os) const override;
};

#endif
