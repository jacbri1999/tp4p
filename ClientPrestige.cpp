/*
* Date : 23 mars 2019
* Auteurs : Matthew Paoli et Jacob Brisson
*/
#include "ClientPrestige.h"
#include "Restaurant.h"

ClientPrestige::ClientPrestige()
{
	adresse_ = ZoneHabitation::Zone3;
}

// Constructeur par parametre de ClientPrestige. ClientPrestige h�rite de ClientRegulier. 
ClientPrestige::ClientPrestige(string_view nom, string_view prenom, int tailleGroupe, int nbPoints, ZoneHabitation adresse): ClientRegulier(nom ,prenom, tailleGroupe, nbPoints), adresse_(adresse)
{
  
}

ZoneHabitation ClientPrestige::getAdresseCode() const
{
	return adresse_;
}



// M�thode qui prend un ostream par reference en parametre et qui affiche les attributs du ClientPrestige. 
// C'est une methode virtuelle, afin que la bonne methode soit appelee en fonction du type de l'objet.
void ClientPrestige::afficherClient(ostream & os) const
{
	ClientRegulier::afficherClient(os);
	os << "Habite dans la zone :" << getAdressCodeString() << endl;
}
string ClientPrestige::getAdressCodeString() const
{
	string zone;
	switch (adresse_)
	{
	case ZoneHabitation::Zone1:
		zone = "Zone 1";
		break;
	case  ZoneHabitation::Zone2:
		zone = " Zone 2";
		break;
	case  ZoneHabitation::Zone3:
		zone = "Zone 3";
		break;

	default:
		zone = "erreur zone";
		break;
	}
	return zone;
}




// Methode qui retourne une reduction qui prend en compte des frais de livraisons, selon si le nombre de points est plus petit ou non que le SEUIL_LIVRAISON_GRATUITE. Elle redefinie la methode de Client.
double ClientPrestige :: getReduction(const Restaurant & res, double montant , bool estLivraison) const
{   
	if (nbPoints_ < SEUIL_LIVRAISON_GRATUITE) {
		if (estLivraison)
			return ((-1 * montant) * TAUX_REDUC_PRESTIGE) + res.getFraisLivraison(adresse_);
	}
	return ((-1 * montant) * TAUX_REDUC_PRESTIGE);

	
}
