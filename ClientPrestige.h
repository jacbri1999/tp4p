/*
* Date : 23 mars 2019
* Auteurs : Matthew Paoli et Jacob Brisson
*/
#ifndef CLIENT_PRESTIGE
#define CLIENT_PRESTIGE

#include "ClientRegulier.h"
#include "def.h"

class ClientPrestige : public ClientRegulier
{
public:
	//Constructeurs 
	ClientPrestige();
	ClientPrestige(string_view nom, string_view prenom, int tailleGroupe, int nbPoints, ZoneHabitation adresse);
	virtual ~ClientPrestige() = default;

	//Accesseur 
	ZoneHabitation getAdresseCode() const;

	//Autres Fonctions
	void afficherClient(ostream & os) const override;
	string getAdressCodeString() const;
	double getReduction(const Restaurant & res, double montant, bool estLivraison) const override; 

private:
	ZoneHabitation adresse_;

};

#endif

