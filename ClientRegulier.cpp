/*
* Date : 23 mars 2019
* Auteurs : Matthew Paoli et Jacob Brisson
*/
#include "ClientRegulier.h"

ClientRegulier::ClientRegulier()
{
	nbPoints_ = 0;
}


// Constructeur par parametre de ClientRegulier. ClientRegulier h�rite de Client et possede un attribut de plus : nbPoints. 
ClientRegulier::ClientRegulier(string_view nom, string_view prenom, int tailleGroupe, int npoints): Client(nom, prenom, tailleGroupe), nbPoints_(npoints)
{ 
}


// Methode qui retourne le nombre de points du client. Elle redefinie la methode de Client.
int ClientRegulier::getNbPoints() const
{
	return nbPoints_;
}


void ClientRegulier::augmenterNbPoints(int bonus)
{
	nbPoints_ += bonus;
}


// M�thode qui prend un ostream par reference en parametre et qui affiche les attributs du ClientRegulier. 
// C'est une methode virtuelle, afin que la bonne methode soit appelee en fonction du type de l'objet.
void ClientRegulier::afficherClient(ostream & os) const
{
	Client::afficherClient(os);
	os << "Possede " << nbPoints_ << " points" << endl;
}


// Methode qui retourne une reduction en fonction du nombre de points du client : la reduction est appliquee si ce nombre de points est plus eleve que le SEUIL_DEBUT_REDUCTION. Elle redefinie la methode de Client.
double ClientRegulier::getReduction(const Restaurant & res, double montant, bool estLivraison) const
{
	if (nbPoints_ > SEUIL_DEBUT_REDUCTION)
		return ((-1 * montant) * TAUX_REDUC_REGULIER);
	else
		return 0.0;
	
}
