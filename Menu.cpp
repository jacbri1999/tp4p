/*
* Date : 23 mars 2019
* Auteurs : Matthew Paoli et Jacob Brisson
*/

#include "Menu.h"
#include "LectureFichierEnSections.h"
#include <cassert>  //? Contient "assert" qui permet de s'assurer qu'une expression est vraie, et terminer le programme avec un message d'erreur en cas contraire.
using namespace std;

// Constructeurs.

Menu::Menu() :
	type_{ TypeMenu::Matin }
{
}

Menu::Menu(string fichier, TypeMenu type) :
	type_{ type }
{
	lireMenu(fichier);
}

// Le destructeur de Menu prend rien en parametre et desalloue tous les pointeur dans la liste de plats
// et la liste de plats vege
Menu::~Menu()
{
	for (int i = 0; i < listePlats_.size(); ++i) // Parcours tous les elements de listePlats_, qui contient tous les types de plats 
		delete listePlats_[i];
	listePlats_.clear(); // Clear le vecteur
	listePlatsVege_.clear(); // Clear le vecteur 
}

Plat* Menu::allouerPlat(Plat* plat) {
	return plat->clone();
}

// Le constructeur de copie de Menu prend en parametre l'adresse d'un menu et a pour but de copier un menu.
Menu::Menu(const Menu & menu) : type_(menu.type_)
{
	for (int i = 0; i < menu.listePlats_.size(); ++i) // Parcours tous les pointeurs de Plat pr�sents dans le vecteurs listePlats_ 
		*this += allouerPlat(menu.listePlats_[i]); // On ajoute tous les pointeurs de Plat au menu en appelant l'operateur += et en l'ajoutant au menu
}

// La surchage de l'operatuer = prend en parametre la reference d'un menu et a pour fonction d'ecraser les anciens attributs de Menu par le nouveau.
Menu & Menu::operator=(const Menu & menu)
{
	if (this != &menu) { // Verifie si on n'essaie pas de copier la meme chose
		type_ = menu.type_;
		for (int i = 0; i < listePlats_.size(); ++i) // On desalloue tous les pointeurs de Plat dans l'ancien menu
			delete listePlats_[i];
		listePlats_.clear();
		for (int j = 0; j < menu.listePlats_.size(); ++j) // On ajoute tous les nouveaux pointeurs de Plat au menu
			*this += allouerPlat(menu.listePlats_[j]); // Fait appelle a l'operateur += afin d'ajoute le pointeur au menu en faisant une copie grace a allouerPlat qui fait un clone pointeur.

		for (int k = 0; k < listePlatsVege_.size(); ++k) // On desalloue tous les pointeurs de PlatVege dans l'ancien menu
			delete listePlatsVege_[k];
		listePlatsVege_.clear();
		for (int l = 0; l < menu.listePlatsVege_.size(); ++l) // On ajoute tous les nouveau pointeurs de PlatVege au menu
			*this += allouerPlat(dynamic_cast <PlatVege*> (menu.listePlatsVege_[l])); // Afin d'appeller allouerPlat on doit passer un pointeur de plat alors on dynamique_cast
																					  // le pointeurs de PlatVege en Plat. Par la suite on utilise l'operateur += afin d'ajouter le pointeur au menu
																					  // en faisant une copie grace a allouerPlat qui fait un clonde du pointeur.
	}
	return *this;
}

// Getters.

vector<Plat*> Menu::getListePlats() const
{
	return listePlats_;
}

// Autres methodes.

// La surchage de l'operateur += prend en parametre un pointeur de la classe Plat et a pour but d'ajouter
// ce pointeur au deux vecteurs ( listePlatsVege et listePlats)
Menu& Menu::operator+=(owner<Plat*> plat) {
	if (dynamic_cast<PlatVege*> (plat)) { // Si le pointeur recu est un PlatVege, la condition est vrai et on entre dans le if
		listePlatsVege_.push_back(dynamic_cast<PlatVege*> (plat)); // Dynamique_cast le pointeur de plat en PlatVege et on l'ajoute au vecteur 
		listePlats_.push_back(dynamic_cast<PlatVege*>(plat)); // Dynamique_cast le pointeur de plat en PlatVege et on l'ajoute au vecteur
		return *this; // On retourne le pointeur afin d'�tre capable d'appeller cette fonction en cascade
	}
	else if (dynamic_cast<PlatBioVege*> (plat)) { // Si le pointeur recu est un PlatBioVege, la condition est vrai et on entre dans le if
		listePlatsVege_.push_back(dynamic_cast<PlatBioVege*> (plat)); // Dynamique_cast le pointeur de plat en PlatBioVege et on l'ajoute au vecteur
		listePlats_.push_back(dynamic_cast<PlatBioVege*>(plat)); // Dynamique_cast le pointeur de plat en PlatBioVege et on l'ajoute au vecteur
		return *this; // Retourne this nous permet de l'appeler en cascade
	}
	else { // Si le pointeur n'est pas un PlatVege ou un PlatBioVege on rentre dans cette condition
		listePlats_.push_back(plat); // On ajoute le pointeur de plat directement car il s'agit deja de la bonne classe
		return *this; // Retourne this nous permet de l'appeler en cascade
	}


}

void Menu::lireMenu(const string& nomFichier) {
	LectureFichierEnSections fichier{ nomFichier };
	fichier.allerASection(entetesDesTypesDeMenu[static_cast<int>(type_)]);
	while (!fichier.estFinSection())
		*this += lirePlatDe(fichier);
}

Plat* Menu::trouverPlatMoinsCher() const
{
	assert(!listePlats_.empty() && "La liste de plats de doit pas etre vide pour trouver le plat le moins cher.");
	Plat* minimum = listePlats_[0];
	for (Plat* plat : listePlats_)
		if (*plat < *minimum)
			minimum = plat;

	return minimum;
}

Plat* Menu::trouverPlat(string_view nom) const
{
	for (Plat* plat : listePlats_)
		if (plat->getNom() == nom)
			return plat;

	return nullptr;
}
Plat* Menu::lirePlatDe(LectureFichierEnSections& fichier)
{
	auto lectureLigne = fichier.lecteurDeLigne();

	string nom, typeStr;
	TypePlat type;
	double prix, coutDeRevient;
	lectureLigne >> nom >> typeStr >> prix >> coutDeRevient;
	type = TypePlat(stoi(typeStr));
	double ecotaxe, vitamines, proteines, mineraux;
	switch (type) {
	case TypePlat::Bio:
		lectureLigne >> ecotaxe;
		return new PlatBio{ nom, prix, coutDeRevient, ecotaxe };
	case TypePlat::BioVege:
		lectureLigne >> ecotaxe >> vitamines >> proteines >> mineraux;
		return new PlatBioVege(nom, prix, coutDeRevient, ecotaxe, vitamines, proteines, mineraux);
	case TypePlat::Vege:
		lectureLigne >> vitamines >> proteines >> mineraux;
		return new PlatVege(nom, prix, coutDeRevient, vitamines, proteines, mineraux);
	default:
		return new Plat{ nom, prix, coutDeRevient };
	}

}

// Fonctions generales.

// La surchage d'operateur << prend en parametre un ostream par reference ainsi qu'une referance a un Menu. Le but 
// de cette methode est d'afficher les caracteristique du menu.
ostream& operator<<(ostream& os, const Menu& menu)
{
	for (int i = 0; i < menu.listePlats_.size(); ++i) // La boucle parcours toutes les pointeurs de plat dans listePlats
		menu.listePlats_[i]->afficherPlat(os); // On appelle la methode afficherPlat afin d'afficher les caracteristiques des plats
	os << endl << "MENU ENTIEREMENT VEGETARIEN" << endl;
	for (int j = 0; j < menu.listePlatsVege_.size(); ++j) // La boucle parcours toutes les pointeurs de plat dans listePlatsVeges
		menu.listePlatsVege_[j]->afficherVege(os); // On appelle la methode afficherPlat de PlatVege afin d'afficher les caracteristiques des plats

	return os;
}
