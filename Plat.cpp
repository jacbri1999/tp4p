/*
* Date : 23 mars 2019
* Auteurs : Matthew Paoli et Jacob Brisson
*/

#include "Plat.h"
#include "PlatBio.h"
#include "PlatVege.h"
#include "PlatBioVege.h"
//#include "debogageMemoire.hpp"
using namespace std;

Plat::Plat(string_view nom, double prix, double cout) : nom_(nom), prix_(prix), cout_(cout) {
}

string Plat::getNom() const {
	return nom_;
}

double Plat::getPrix() const {
	return prix_;
}

double Plat::getCout() const {
	return cout_;
}
//setters 
void Plat::setNom(string nom) {
	nom_ = nom;
}

void Plat::setPrix(double prix) {
	prix_ = prix;
}

bool Plat::operator < (const Plat& plat) const
{
	return prix_ < plat.prix_;
}

// La methode afficherPlat prend en parametre un ostream par reference et a pour fonction d'afficher les informations
// d'un plat. Cette fonction est definie comme virtual afin que la bonne methdoe soit appeller en fonction du type de l'objet.
void Plat::afficherPlat(ostream & os) const
{
	os << "PLAT ----" << nom_ << " - " << prix_ << " $ (" << cout_ << "$ pour le restaurant)" << endl;
}

// La methode getPrixRevient prend aucune valeur en parametre et retourne la valeur du montant que le restaurant fait 
// Cette methode est definie comme etant virutal afin que la bonne methdoe soit appeller en fonction du type de l'objet.
double Plat::getPrixRevient() const
{
	return prix_ - cout_;
}

// La methode clone prend aucun parametre et retourne un pointeur vers un Plat. Un nouveau pointeur de Plat est creer
// avec les attributs privees de la classe Plat et ce pointeur est retournee.
Plat *  Plat::clone() const
{
	Plat* platCopie = new Plat(nom_, prix_, cout_);
	return platCopie;
}
