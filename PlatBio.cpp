/*
* Date : 23 mars 2019
* Auteurs : Matthew Paoli et Jacob Brisson
*/
#include "PlatBio.h"

// Constructeur par parametre de PlatBio qui herite de plat ainsi que l'attribut supplementaire ecoTaxe_.
PlatBio::PlatBio(string nom, double prix, double cout, double ecotaxe) : Plat(nom, prix, cout), ecoTaxe_(ecotaxe)
{
}

double PlatBio::getEcoTaxe() const
{
	return ecoTaxe_;
}

void PlatBio::setEcoTaxe(double ecoTaxe)
{
	ecoTaxe_ = ecoTaxe;
}

// La methode clone prend aucun parametre et retourne un pointeur vers un Plat. Un nouveau pointeur de PlatBio est creer
// avec les attributs privees de la classe PlatBio et ce pointeur est retournee. Cette methode est override puisqu'on redefnie
// la methode.
Plat * PlatBio::clone() const
{
	Plat* platCopie = new PlatBio(nom_, prix_, cout_, ecoTaxe_);
	return platCopie;
}

// La methode getPrixrevient prend rien en parametre et retourne le montant fait par le restaurant. Cette methode
// est override puisqu'on redefinie cette methode afin de prendre en consideration l'ecotaxe.
double PlatBio::getPrixRevient() const
{
	return prix_ - cout_ + ecoTaxe_;
}

// La methode afficherPlat prend un ostream par reference en parametre et a pour fonction d'afficher les informatiosn
// de PlatBio. L'affichage de Plat est appeller en premier et par la suite les nouvelles informations pour PlatBio 
// sont ajoutees. Cette methode est override puisqu'on redefinie la methode d'afficherPlat.
void PlatBio::afficherPlat(ostream& os) const
{
	Plat::afficherPlat(os);
	os << "Plat Bio   comprend une Taxe ecologique de :" << ecoTaxe_ << "$" << endl;
}


