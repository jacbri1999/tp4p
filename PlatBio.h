/*
* Date : 23 mars 2019
* Auteurs : Matthew Paoli et Jacob Brisson
*/

#ifndef PLAT_BIO_H
#define PLAT_BIO_H

#include <string>
#include <iostream>
#include "Plat.h"

using namespace std;

class PlatBio : public Plat {
public:
	// constructeurs
	PlatBio(string nom = "inconnu", double prix = 0, double cout = 0, double ecotaxe = 0);
	virtual ~PlatBio() = default; // Le destructeur de PlatBio est definie comme etant virtual puisque dependamment 
								  // de l'objet, il faut appeller le bon destructeur de cette classe
	//getters 
	double getEcoTaxe() const;
	double getPrixRevient() const override; 
	//setters 
	void setEcoTaxe(double ecoTaxe);

	//autres
	void afficherPlat(ostream& os) const override; 
	Plat * clone() const override; 
protected:
	double ecoTaxe_;

};

#endif // !PLAT_VERT_H
