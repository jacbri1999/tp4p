/*
* Date : 23 mars 2019
* Auteurs : Matthew Paoli et Jacob Brisson
*/
#include "PlatBioVege.h"

// Constructeur par parametre de PlatBioVege qui herite de PlatBio ainsi que de Vege.
PlatBioVege::PlatBioVege(string nom, double prix, double cout, double ecotaxe, double vitamines, double proteines, double mineraux) : PlatBio(nom, prix, cout, ecotaxe), Vege(nom, vitamines, proteines, mineraux)
{ 
}

// La methode afficherPlat prend un ostream par reference en parametre et a pour fonction d'afficher les informatiosn
// de PlatBioVege. Puisque PlatBioVege herite de PlatBio et Vege on peut simplement appeller afficherPlat de PlatBio
// ainsi que de Vege. Cette methode est override puisqu'on redefinie la methode d'afficherPlat.
void PlatBioVege::afficherPlat(ostream & os) const
{   
	PlatBio::afficherPlat(os);
	os << "ET ";
	Vege::afficherVege(os);

}

// La methode clone prend aucun parametre et retourne un pointeur vers un Plat. Un nouveau pointeur de PlatBioVege est creer
// avec les attributs privees de la classe PlatBioVege et ce pointeur est retournee. Cette methode est override puisqu'on redefnie
// la methode.
Plat * PlatBioVege::clone() const
{ 
	Plat* platCopie = new PlatBioVege(PlatBio::nom_, prix_, cout_, ecoTaxe_, vitamines_, proteines_, mineraux_);
	return platCopie;
}

// La methode calculerApportNutritif prend rien en parametre et retourne un double qui represente l'apportNutritif. Cette 
// methode est redefinie puisqu'elle comprend des attributs non defenie dans la classe Vege.
double PlatBioVege::calculerApportNutritif() const
{
	
	return (vitamines_ * proteines_ / mineraux_) * RAPPORT_NUTRITIF_BIO * AVANTAGE_SANS_PESTICIDE;
}
