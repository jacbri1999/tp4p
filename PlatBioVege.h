/*
* Date : 23 mars 2019
* Auteurs : Matthew Paoli et Jacob Brisson
*/
#ifndef PLAT_BIOVEGE_H
#define PLAT_BIOVEGE_H

#include "PlatBio.h"
#include "Vege.h"
#include "def.h"
class PlatBioVege :
	public PlatBio, public Vege
{
public:
	//Constructeurs
	PlatBioVege(string nom = "inconnu", double prix = 0, double cout = 0, double ecotaxe = 0, double vitamines = 0, double proteines = 0, double mineraux = 0);
	virtual ~PlatBioVege() = default; // Le destructeur de PlatBioVege est definie comme etant virtual puisque dependamment 
									  // de l'objet, il faut appeller le bon destructeur de cette classe

	//Autres
	Plat * clone() const override; 
	void afficherPlat(ostream & os) const override;
	double calculerApportNutritif() const override; 

};
#endif
