/*
* Date : 23 mars 2019
* Auteurs : Matthew Paoli et Jacob Brisson
*/
#include "PlatVege.h"


// Constructeur par parametre de PlatVege qui herite de Plat et Vege et qui appelle setTaxe() afin de determiner la valeur de la taxe
PlatVege::PlatVege(string nom, double prix, double cout, double vitamines, double proteines, double mineraux) : Plat(nom, prix, cout), Vege(nom, vitamines, proteines, mineraux)
{
	setTaxe();
}

// La methode clone prend aucun parametre et retourne un pointeur vers un Plat. Un nouveau pointeur de PlatVege est creer
// avec les attributs privees de la classe PlatVege et ce pointeur est retournee. Cette methode est override puisqu'on redefnie
// la methode.
Plat* PlatVege::clone() const
{ 
	Plat* platCopie = new PlatVege(Plat::nom_, prix_, cout_, vitamines_, proteines_, mineraux_);
	return platCopie;
}

// La methode afficherPlat prend un ostream par reference en parametre et a pour fonction d'afficher les informatiosn
// de PlatVege. Puisque PlatVege herite de Plat et Vege on peut simplement appeller afficherPlat de Plat
// ainsi que de Vege. Cette methode est override puisqu'on redefinie la methode d'afficherPlat. De plus on appelle
// calculerApportNutritif afin d'obtenir la valeur de l'apport nutritif.
void PlatVege::afficherPlat(ostream & os) const
{   
	Plat::afficherPlat(os);
	Vege::afficherVege(os);
	os << "(Apport nutritif " << calculerApportNutritif() << "mg)" << endl;
}

// La methode calculerApportNutritif prend rien en parametre et retourne un double qui represente l'apportNutritif. Cette 
// methode est redefinie puisqu'elle comprend des attributs non defenie dans la classe Vege.
double PlatVege::calculerApportNutritif() const
{
	
	return Vege::calculerApportNutritif() *RAPPORT_NUTRITIF;
}

// La methode getTaxe prend rien en parametre et retourne la valeur de l'attribut privee de taxe_. Cette methode est
// override puisqu'on la redefinie
double PlatVege::getTaxe() const
{
	return taxe_;
}

// La methode setTaxe prend rien en parametre et a comme fonction de set la valeur de la taxe a 0.07. Cette methode est
// override puisqu'on la redefinie.
void PlatVege::setTaxe()
{
	taxe_ = 0.07;
}
