/*
* Date : 23 mars 2019
* Auteurs : Matthew Paoli et Jacob Brisson
*/
#ifndef PLAT_VEGE_H
#define PLAT_VEGE_H

#include "Vege.h"
#include "Plat.h"
#include "def.h"
#include "Taxable.h"
class PlatVege :
	public Vege, public Plat, public Taxable
{
public:
	//Constructeurs
	PlatVege(string nom = "inconnu", double prix = 0, double cout = 0, double vitamines = 0, double proteines = 0, double mineraux = 0);//TODO
	virtual ~PlatVege() = default; // Le destructeur de PlatVege est definie comme etant virtual puisque dependamment 
								   // de l'objet, il faut appeller le bon destructeur de cette classe
	//Getters
	double getTaxe() const override;
	//Setters
	void setTaxe() override;
	//Autres
	Plat * clone() const override; 
	void afficherPlat(ostream & os) const override;
	double calculerApportNutritif() const override; 

protected:
	double taxe_;
};
#endif
