/*
* Date : 23 mars 2019
* Auteurs : Matthew Paoli et Jacob Brisson
*/
#include "Vege.h"

Vege::Vege(string nom, double vitamines, double proteines, double mineraux) : nom_(nom), vitamines_(vitamines), proteines_(proteines), mineraux_(mineraux)
{
}

double Vege::getVitamines() const
{
	return vitamines_;
}

double Vege::getProteines() const
{
	return proteines_;
}

double Vege::getMineraux() const
{
	return mineraux_;
}

void Vege::setVitamines(double vitamines)
{
	vitamines_ = vitamines;
}

void Vege::setProteines(double proteines)
{
	proteines_ = proteines;
}

void Vege::setMineraux(double mineraux)
{
	mineraux_ = mineraux;
}

// La methode afficherVege prend en parametre un ostream par reference et affiche les informations veges. Cette methode est 
// definie comme etant virtual afin que la bonne methdoe soit appeller en fonction du type de l'objet.
void Vege::afficherVege(ostream & os) const
{
	os << "PLAT VEGE " << nom_ << " vitamines " << vitamines_ << " Proteines " << proteines_ << " Mineraux " << mineraux_ << endl;

}

// La methode calculerApportNutritif prend aucun parametre et retourne un double qui represente la valeur de l'apport
// nutritif. Cette fonction est definie comme etant virtual pure et par consequent cette classe est abstraite
// De plus, si un objet d'une autre classe tente d'utiliser cette methode, il faut la redefinir a chaque fois.
double Vege::calculerApportNutritif() const
{
	return (vitamines_ * proteines_ / mineraux_);
}


