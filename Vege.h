/*
* Date : 23 mars 2019
* Auteurs : Matthew Paoli et Jacob Brisson
*/
#ifndef VEGE_H
#define VEGE_H
#include <iostream>
#include <string_view>
#include <string>
#include"Taxable.h"

using namespace std;
class Vege
{
public:
	//Constructeurs
	Vege(string nom, double vitamines, double proteines, double mineraux);
	~Vege() = default;
	//Getters
	double getVitamines() const;
	double getProteines() const;
	double getMineraux() const;
	//Setters
	void setVitamines(double vitamines);
	void setProteines(double proteines);
	void setMineraux(double mineraux);
	//Autres
    virtual void afficherVege(ostream & os)const; 
    virtual double calculerApportNutritif() const =0; //Virtuelle pure, afin de definir la classe comme abstraite.  

protected: 
	double vitamines_;
	double proteines_;
	double mineraux_;
    
    string nom_;
};
#endif
